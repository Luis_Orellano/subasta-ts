import { Observer } from "./Observer.js"; 

interface Subject {
  attach(observer: Observer): void;

  detach(observer: Observer): void;

  notify(): void;
}
export { Subject };
