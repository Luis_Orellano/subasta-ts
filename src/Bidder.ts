import { Subject } from "./Subject.js";
import { Auctioneer } from "./Auctioneer.js";
import { Observer } from "./Observer.js";

class Bidder implements Observer {
  nombre;

  constructor(nombre?: string) {
    this.nombre = nombre;
  }

  public update(subject: Subject): void {
    if (!(subject instanceof Auctioneer)) {
      throw new Error("Error: el subject no es un auctioneer");
    }

    if (subject.getProduct()!.observer === this) {
      return console.log(
        `${this.getNombre()} es el dueño de momento... Esperando otras pujas`
      );
    }

    console.log(
      `${this.getNombre()} no es el dueño aun... esta pensando en que deveria apostar`
    );

    const puja = Math.round(subject.getProduct()!.pujaInicial! * 1.1);
    // if (puja > this.maxPuja) {
    //   return console.log(`${this.getNombre()}: La oferta es superior al limete de puja. El limite de puja es: ${this.maxPuja}$`);
    // }
    subject.pujar(this, puja);
  }

  public getNombre(): string | undefined{
    return this.nombre;
  }

  public setNombre(nombre: string): void {
    this.nombre = nombre;
  }
}
export { Bidder };
