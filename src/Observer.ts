import { Subject } from "./Subject.js"; 

interface Observer {
  nombre?: string;
  update(subject: Subject): void;
}
export { Observer };
