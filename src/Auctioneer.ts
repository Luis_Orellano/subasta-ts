import { Observer } from "./Observer.js";
import { Subject } from "./Subject.js";
import { Product } from "./Product.js";

class Auctioneer implements Subject {
  private product!: Product;

  private observers: Observer[] = [];

  /**
   * Metodos de subscripcion a la subasta
   */
  //Metodo para agregar apostadores a la subasta
  public attach(observer: Observer): void {
    const isExist = this.observers.includes(observer);
    if (isExist) {
      return console.log("Ya existe este apostador");
    }
    console.log("Se agrego el apostador a la subasta");
    this.observers.push(observer);
  }

  //Metodo para quitar apostadores de la subasta
  public detach(observer: Observer): void {
    const index = this.observers.indexOf(observer);
    if (index === -1) {
      return console.log("No existe ese apostador");
    }

    this.observers.splice(index, 1);
    console.log(`El apostador ${observer.nombre} fue quitado de la subasta`);
  }

  //Metodo para notificar a cada apostador
  public notify(): void {
    console.log("notificacion para cada apostador");
    for (const observer of this.observers) {
      observer.update(this);
    }
  }

  public getProduct(): Product | undefined{
    return this.product;
  }

  public setProduct(product: Product): void {
    this.product = product;
  }

  public pujar(observer: Observer, puja: number): void {
    console.log("Se registro una puja");
    const isExist = this.observers.includes(observer);
    if (!isExist) {
      console.log("No existe apostador en el sistema");
    }
    if (this.product!.pujaInicial! >= puja) {
      console.log("PUJA: ", puja);
      console.log("Precio del producto: ", this.product!.pujaInicial);
      return console.log(`La puja de ${observer.nombre}, no es valida`);
    }
    this.product!.pujaInicial = puja;
    this.product!.observer = observer;

    console.log(
      `El nuevo precio para pujar es ${puja} y el nuevo dueño es ${observer.nombre}`
    );
    this.notify();
  }
}
export { Auctioneer };
