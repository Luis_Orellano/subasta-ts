import { Observer } from "./Observer.js";

class Product {
  public nombre?: string;
  public pujaInicial?: number;
  public pujaDeAumento?: number;
  public duracion?: number;
  public observer?: Observer;

  constructor(
    product: Product
  ) {
    this.nombre = product.nombre || "Unknown";
    this.pujaInicial = product.pujaInicial || 0;
    this.pujaDeAumento = product.pujaDeAumento || 100;
    this.duracion = product.duracion || 10;
  }

  public tiempoPuja(): boolean {
    if (this.duracion! > 0) {
      let intervalId = setInterval(() => {
        this.duracion = this.duracion! - 1;
        console.log(this.duracion);
        if (this.duracion === 0) clearInterval(intervalId);
      }, 1000);
      return true;
    }
    return false;
  }

  // public getPujaInicial(): number {
  //   return this.pujaInicial;
  // }

  // public getPujaDeAumento(): number {
  //   return this.pujaDeAumento;
  // }

  // public getDuracion(): number {
  //   return this.duracion;
  // }

  // public getNombre(): string | undefined {
  //   return this.nombre;
  // }

  // public getObserver(): Observer | undefined{
  //   return this.observer;
  // }

  // public setPujaInicial(pujaInicial: number): void {
  //   this.pujaInicial = pujaInicial;
  // }

  // public setPujaDeAumento(pujaDeAumento: number): void {
  //   this.pujaDeAumento = pujaDeAumento;
  // }

  // public setDuracion(duracion: number): void {
  //   this.duracion = duracion;
  // }

  // public setNombre(nombre: string): void {
  //   this.nombre = nombre;
  // }

  // public setObserver(observer: Observer): void {
  //   this.observer = observer;
  // }
}
export { Product };
