import { Auctioneer } from "./Auctioneer.js";
import { Bidder } from "./Bidder.js";
import { Product } from "./Product.js";

const auctioneers: Auctioneer[] = new Array();
const auctioneer1 = new Auctioneer();

const bidders: Bidder[] = new Array();
const bidder1 = new Bidder("Luis");
const bidder2 = new Bidder("Tito");
const bidder3 = new Bidder("Chiqui");
const bidder4 = new Bidder("Eva");

auctioneers.push(auctioneer1);

bidders.push(bidder1);
bidders.push(bidder2);
bidders.push(bidder3);
bidders.push(bidder4);

bidders.forEach((bidder) => {
  auctioneer1.attach(bidder);
});

const diamond = new Product({ nombre: "DIAMOND", pujaInicial: 100 });
auctioneer1.setProduct(diamond);

console.log("---------Nueva Puja-----------");

auctioneer1.detach(bidder3);

//const gem = new Product("GEMA", 250, 200);
console.log(auctioneer1.getProduct())

auctioneer1.pujar(bidder1, 10);


 if (auctioneer1.getProduct()!.tiempoPuja()) {
  auctioneer1.pujar(bidder1, 50);

  console.log(`El ganador de la puja por ${diamond.nombre} es:
            Nombre: ${diamond.observer!.nombre}
            Precio: ${diamond.pujaInicial}`);

  // console.log(`El ganador de la puja por ${gem.getNombre()} es:
  //           Nombre: ${gem.getObserver().nombre}
  //           Precio: ${gem.getPujaInicial()}`);
}
// class Timer {
//   constructor(public counter = 10) {
//     let intervalId = setInterval(() => {
//       this.counter = this.counter - 1;
//       console.log(this.counter);
//       if (this.counter === 0) clearInterval(intervalId);
//     }, 1000);
//   }
// }

// const timer = new Timer();
