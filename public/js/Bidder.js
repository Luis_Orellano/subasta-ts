import { Auctioneer } from "./Auctioneer.js";
class Bidder {
    constructor(nombre) {
        this.nombre = nombre;
    }
    update(subject) {
        if (!(subject instanceof Auctioneer)) {
            throw new Error("Error: el subject no es un auctioneer");
        }
        if (subject.getProduct().observer === this) {
            return console.log(`${this.getNombre()} es el dueño de momento... Esperando otras pujas`);
        }
        console.log(`${this.getNombre()} no es el dueño aun... esta pensando en que deveria apostar`);
        const puja = Math.round(subject.getProduct().pujaInicial * 1.1);
        // if (puja > this.maxPuja) {
        //   return console.log(`${this.getNombre()}: La oferta es superior al limete de puja. El limite de puja es: ${this.maxPuja}$`);
        // }
        subject.pujar(this, puja);
    }
    getNombre() {
        return this.nombre;
    }
    setNombre(nombre) {
        this.nombre = nombre;
    }
}
export { Bidder };
