class Product {
    constructor(product) {
        this.nombre = product.nombre || "Unknown";
        this.pujaInicial = product.pujaInicial || 0;
        this.pujaDeAumento = product.pujaDeAumento || 100;
        this.duracion = product.duracion || 10;
    }
    tiempoPuja() {
        if (this.duracion > 0) {
            let intervalId = setInterval(() => {
                this.duracion = this.duracion - 1;
                console.log(this.duracion);
                if (this.duracion === 0)
                    clearInterval(intervalId);
            }, 1000);
            return true;
        }
        return false;
    }
}
export { Product };
